import fetch from 'dva/fetch';

// const baseUrl = '';
// const baseHeader = {
//     'Content-Type': 'application/json',
// };

function parseJson(response) {
    return response.json();
}

/** 
 * @param {string} url
 * @param {object} options
 */
function requestTool(url) {
    return fetch(url)
        .then(parseJson)
        .then(data => (data))
        .catch(err => ({ err }));
}

function request() {
    const finalUrl = 'https://dog.ceo/api/breeds/image/random';
    return requestTool(finalUrl);
}

const getUser = () => {
    return request();
};

const api = {
    getUser
};

export default api;
