import Link from 'next/link';
import WithDva from '../utils/store';
import React, { Component } from 'react'
import Head from 'next/head'

export class users extends Component {
    static async getInitialProps(props) {
        const {
            pathname, query, isServer, store,
        } = props;

        await props.store.dispatch({ type: 'index/fetch' });
        return {
            pathname, query, isServer, dvaStore: store,
        };
    }

    render() {
        const { index } = this.props
        const { imageURL } = index

        return (
            <div>
                <Head>
                    <meta name="title" content="Demo Example" />
                    <meta name="description" content='Find the best demos' />
                </Head>
                <div>
                    <img src={imageURL.message} />
                </div>
            </div>
        )
    }
}

export default WithDva((state) => { return { index: state.index }; })(users);