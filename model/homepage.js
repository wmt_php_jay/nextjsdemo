import api from '../utils/api';


const delay = timeout => new Promise(resolve => setTimeout(resolve, timeout));

const { getUser } = api;


const model = {
  namespace: 'index',
  state: {
    name: 'ABC',
    count: 0,
    init: false,
    imageURL: [],

  },
  reducers: {
    caculate(state, payload) {
      const { count } = state;
      const { delta } = payload;
      return { ...state, count: count + delta };
    },
    save(state, payload) {
      const { imageURL } = payload;
      return { ...state, imageURL: imageURL };
    },

  },
  effects: {
    *init(action, { put }) {
      yield delay(2000);
      yield put({ type: 'caculate', delta: 1 });
    },
    *fetch(action, { put, call }) {
      const res = yield call(getUser);
      yield put({ type: 'save', imageURL: res });
    },
  },
};

export default model;
